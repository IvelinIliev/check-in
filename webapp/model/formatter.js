sap.ui.define([], function () {
	"use strict";

	return {
		/**
		 * Rounds the currency value to 2 digits
		 *
		 * @public
		 * @param {string} sValue value to be formatted
		 * @returns {string} formatted currency value with 2 digits
		 */
		currencyValue: function (sValue) {
			if (!sValue) {
				return "";
			}

			return parseFloat(sValue).toFixed(2);
		},

		dateFormatter: function (jsonDateString) {
			if (!jsonDateString) {
				return "";
			}
			if (typeof jsonDateString !== "object") {
				let newDate = new Date(parseInt(jsonDateString.replace('/Date(', '')))
				let time = newDate.toTimeString().split(" ")[0]
				return `${newDate.toLocaleDateString()}, ${time}`;
			} else {
				let time = jsonDateString.toTimeString().split(" ")[0]
				return `${jsonDateString.toLocaleDateString()}, ${time}`;
			}

		},

		dateFormatterForPicker: function (jsonDateString) {
			if (!jsonDateString) {
				return null;
			}
			if (typeof jsonDateString !== "object") {
				return new Date(parseInt(jsonDateString.replace('/Date(', '')));
			} else {
				return jsonDateString;
			}

		}
	};
});