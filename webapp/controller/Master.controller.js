sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/Sorter",
	"sap/ui/model/FilterOperator",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"sap/ui/core/Fragment",
	"../model/formatter",
	"sap/m/MessageToast"
], function (BaseController, JSONModel, Filter, Sorter, FilterOperator, GroupHeaderListItem, Device, Fragment, formatter, MessageToast) {
	"use strict";

	return BaseController.extend("zfioricheckin.controller.Master", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the master list controller is instantiated. It sets up the event handling for the master/detail communication and other lifecycle tasks.
		 * @public
		 */
		onInit: function () {
			var oTable = this.byId("masterTable"),
				oViewModel = this._createViewModel();

			this._oGroupFunctions = {
				UnitNumber: function (oContext) {
					var iNumber = oContext.getProperty('UnitNumber'),
						key, text;
					if (iNumber <= 20) {
						key = "LE20";
						text = this.getResourceBundle().getText("masterGroup1Header1");
					} else {
						key = "GT20";
						text = this.getResourceBundle().getText("masterGroup1Header2");
					}
					return {
						key: key,
						text: text
					};
				}.bind(this)
			};

			this._oTable = oTable;
			// keeps the filter and search state
			this._oTableFilterState = {
				aFilter: [],
				aSearch: []
			};

			this.setModel(oViewModel, "masterView");

			this.getView().setBusyIndicatorDelay(0);

			this.getRouter().getRoute("master").attachPatternMatched(this._onMasterMatched, this);
			this.getRouter().attachBypassed(this.onBypassed, this);

			this.initSelectedWhNumber()
		},

		initSelectedWhNumber: function () {
			const localStorageNumber = localStorage.getItem("selectedWhNumber")
			const comboBox = this.getView().byId("warehouseComboBox");
			const defaultValue = "DEV1"
			if (localStorageNumber) {
				this.currentChosenWarehouseNumber = localStorageNumber
			} else {
				this.currentChosenWarehouseNumber = defaultValue
			}
			comboBox.setSelectedKey(this.currentChosenWarehouseNumber)
		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */


		/**
		 * Event handler for refresh event. Keeps filter, sort
		 * and group settings and refreshes the list binding.
		 * @public
		 */
		onRefresh: function () {
			this._oTable.getBinding("items").refresh();
		},

		/**
		 * Event handler for the filter, sort and group buttons to open the ViewSettingsDialog.
		 * @param {sap.ui.base.Event} oEvent the button press event
		 * @public
		 */
		onOpenViewSettings: function (oEvent) {
			var sDialogTab = "filter";
			if (oEvent.getSource() instanceof sap.m.Button) {
				var sButtonId = oEvent.getSource().getId();
				if (sButtonId.match("sort")) {
					sDialogTab = "sort";
				} else if (sButtonId.match("group")) {
					sDialogTab = "group";
				}
			}
			// load asynchronous XML fragment
			if (!this.byId("viewSettingsDialog")) {
				Fragment.load({
					id: this.getView().getId(),
					name: "zfioricheckin.view.ViewSettingsDialog",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					this.getView().addDependent(oDialog);
					oDialog.addStyleClass(this.getOwnerComponent().getContentDensityClass());
					oDialog.open(sDialogTab);
				}.bind(this));
			} else {
				this.byId("viewSettingsDialog").open(sDialogTab);
			}
		},

		onLoadDataPress: function (oEvent) {
			let that = this;

			let oModel = new sap.ui.model.odata.ODataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);

			let startDateRaw = that.getView().byId("beginDate").getDateValue();
			let endDateRaw = that.getView().byId("endDate").getDateValue();

			if (!startDateRaw || !endDateRaw) {
				var msg = "Въведете начална и крайна дата";
				MessageToast.show(msg);
			} else {
				let startDate = this._getFormattedDate(startDateRaw);
				let endDate = this._getFormattedDate(endDateRaw);

				var trackingN = new sap.ui.model.Filter("TrackingNo", "EQ", "");
				var planDateFr = new sap.ui.model.Filter("SelectDateFrom", "EQ", startDateRaw);
				var planDateTo = new sap.ui.model.Filter("SelectDateTo", "EQ", endDateRaw);
				var lgNum = new sap.ui.model.Filter("Lgnum", "EQ", this.currentChosenWarehouseNumber);

				that.getView().setBusy(true);
				oModel.read("/RegisterTruckGenSet", {
					filters: [lgNum, trackingN, planDateFr, planDateTo],
					success: function (data, response) {
						var resultData = data.results;
						var modelData = { "items": resultData };
						console.log(modelData);
						that.getView().setBusy(false);
						var oModel = new sap.ui.model.json.JSONModel();
						oModel.setData(modelData);
						that.getView().setModel(oModel);
						that.quickFilterOnTable({}, "pending");
						// let msg = JSON.parse(response.headers["sap-message"]).message;
						// msg && MessageToast.show(msg);
					}
				});


			}
		},

		onCompanyPress: function (oEvent) {
			this.sortData(oEvent.getSource().getProperty("text"));
		},

		onCarNumberPress: function (oEvent) {
			this.sortData(oEvent.getSource().getProperty("text"));
		},

		onNoPress: function (oEvent) {
			this.sortData(oEvent.getSource().getProperty("text"));
		},

		onDeliveryPress: function (oEvent) {
			this.sortData(oEvent.getSource().getProperty("text"));
		},

		sortData: function (columnName) {
			let obj = {
				"Фирма": "ShipToTxt",
				"Рег. Номер": "TrackingNo",
				"No": "TorId",
				"Вид доставка": "Torcat"
			}

			let oTable = this.getView().byId("masterTable");
			let oItems = oTable.getBinding("items");
			if (oItems) {
				let oBindingPath = obj[columnName];
				let oSorter = new sap.ui.model.Sorter(oBindingPath);
				oItems.sort(oSorter);
			}
		},



		_getFormattedDate(rawDate) {
			let formattedDate = "";
			let months = this._formatValue(rawDate.getMonth() + 1);
			let days = this._formatValue(rawDate.getDate());
			let hours = this._formatValue(rawDate.getHours());
			let minutes = this._formatValue(rawDate.getMinutes());
			let seconds = this._formatValue(rawDate.getSeconds());


			formattedDate = "" + rawDate.getFullYear() + months + days + hours + minutes + seconds;
			return formattedDate;
		},

		_formatValue(rawValue) {
			let resultValue = rawValue;

			if (rawValue < 10) {
				resultValue = "0" + rawValue;
			}

			return resultValue;
		},

		onSearch: function (oEvent) {


			let searchFilter = this.getView().byId("filtersComboBox").getValue();
			var aTableSearchState = [];
			var sQuery = oEvent.getParameter("query");

			if (sQuery && sQuery.length > 0) {

				const filters = {
					"Рег. Номер": new Filter("TrackingNo", FilterOperator.Contains, sQuery),
					"Фирма": new Filter("ShipToTxt", FilterOperator.Contains, sQuery),
					"Статус": new Filter("Status", FilterOperator.Contains, sQuery == "Потвърден" ? "02" : "01")
				};
				//this.quickFilterOnTable();
				aTableSearchState = [filters[searchFilter]];
				//aTableSearchState = [new Filter("TrackingNo", FilterOperator.Contains, sQuery)];
			}
			this._applySearch(aTableSearchState);

			console.log("Search triggered");
		},

		onRefresh: function () {
			var oTable = this.byId("masterTable");
			oTable.getBinding("items").refresh();
		},

		_applySearch: function (aTableSearchState) {
			var oTable = this.byId("masterTable"),
				oViewModel = this.getModel("worklistView");
			try {
				oTable.getBinding("items").filter(aTableSearchState, "Application");
			} catch (error) {
				console.log("No Items in the table!")
			}

			// changes the noDataText of the list in case there are no filter results

			// if (aTableSearchState.length !== 0) {
			// 	oViewModel.setProperty("/tableNoDataText", this.getResourceBundle().getText("worklistNoDataWithSearchText"));
			// }
		},

		/**
		 * Event handler called when ViewSettingsDialog has been confirmed, i.e.
		 * has been closed with 'OK'. In the case, the currently chosen filters, sorters or groupers
		 * are applied to the master list, which can also mean that they
		 * are removed from the master list, in case they are
		 * removed in the ViewSettingsDialog.
		 * @param {sap.ui.base.Event} oEvent the confirm event
		 * @public
		 */
		onConfirmViewSettingsDialog: function (oEvent) {
			var aFilterItems = oEvent.getParameters().filterItems,
				aFilters = [],
				aCaptions = [];

			// update filter state:
			// combine the filter array and the filter string
			aFilterItems.forEach(function (oItem) {
				switch (oItem.getKey()) {
					case "Filter1":
						aFilters.push(new Filter("UnitNumber", FilterOperator.LE, 100));
						break;
					case "Filter2":
						aFilters.push(new Filter("UnitNumber", FilterOperator.GT, 100));
						break;
					default:
						break;
				}
				aCaptions.push(oItem.getText());
			});

			this._oTableFilterState.aFilter = aFilters;
			this._updateFilterBar(aCaptions.join(", "));
			this._applyFilterSearch();
			this._applySortGroup(oEvent);
		},

		/**
		* Apply the chosen sorter and grouper to the master list
		* @param {sap.ui.base.Event} oEvent the confirm event
		* @private
		*/
		_applySortGroup: function (oEvent) {
			var mParams = oEvent.getParameters(),
				sPath,
				bDescending,
				aSorters = [];
			// apply sorter to binding
			// (grouping comes before sorting)
			if (mParams.groupItem) {
				sPath = mParams.groupItem.getKey();
				bDescending = mParams.groupDescending;
				var vGroup = this._oGroupFunctions[sPath];
				aSorters.push(new Sorter(sPath, bDescending, vGroup));
			}
			sPath = mParams.sortItem.getKey();
			bDescending = mParams.sortDescending;
			aSorters.push(new Sorter(sPath, bDescending));
			this._oTable.getBinding("items").sort(aSorters);
		},

		/**
		 * Event handler for the list selection event
		 * @param {sap.ui.base.Event} oEvent the list selectionChange event
		 * @public
		 */
		onSelectionChange: function (oEvent) {
			var oTable = oEvent.getSource(),
				bSelected = oEvent.getParameter("selected");

			// skip navigation when deselecting an item in multi selection mode
			if (!(oTable.getMode() === "MultiSelect" && !bSelected)) {
				// get the list item, either from the listItem parameter or from the event's source itself (will depend on the device-dependent mode).
				var selItem = oEvent.getSource();
				var selData = selItem.oBindingContexts.undefined.oModel.getProperty(selItem.oBindingContexts.undefined.sPath);
				var newModel = new JSONModel();
				newModel.setData(selData);
				this.getOwnerComponent().setModel(newModel, "selected");
				this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
			}
		},

		handleSelectionChange: function (oEvent) {
			var oTable = oEvent.getSource(),
				bSelected = oEvent.getParameter("selected");

			// skip navigation when deselecting an item in multi selection mode
			if (!(oTable.getMode() === "MultiSelect" && !bSelected)) {
				// get the list item, either from the listItem parameter or from the event's source itself (will depend on the device-dependent mode).
				var selItem = oEvent.getParameter("listItem");
				var selData = selItem.oBindingContexts.undefined.oModel.getProperty(selItem.oBindingContexts.undefined.sPath);
				var newModel = new JSONModel();
				newModel.setData(selData);
				this.getOwnerComponent().setModel(newModel, "selected");
				this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
			}
		},

		/**
		 * Event handler for the bypassed event, which is fired when no routing pattern matched.
		 * If there was an object selected in the master list, that selection is removed.
		 * @public
		 */
		onBypassed: function () {
			this._oTable.removeSelections(true);
		},


		/**
		 * Event handler for navigating back.
		 * We navigate back in the browser historz
		 * @public
		 */
		onNavBack: function () {
			// eslint-disable-next-line sap-no-history-manipulation
			history.go(-1);
		},

		/* =========================================================== */
		/* begin: internal methods                                     */
		/* =========================================================== */
		_createViewModel: function () {
			return new JSONModel({
				isFilterBarVisible: false,
				filterBarLabel: "",
				delay: 0,
				title: this.getResourceBundle().getText("masterTitleCount", [0]),
				noDataText: this.getResourceBundle().getText("masterListNoDataText"),
				sortBy: "Name",
				groupBy: "None"
			});
		},
		_onMasterMatched: function () {
			//Set the layout property of the FCL control to 'OneColumn'
			if (this.getModel("appView")) {
				this.getModel("appView").setProperty("/layout", "OneColumn");
			}
			try {
				this.onLoadDataPress();
			} catch (error) {
				console.warn(error);
			}

			this.getView().setModel(new JSONModel(
				{
					filterData: [
						{ val: "Рег. Номер" },
						{ val: "Фирма" },
						{ val: "Статус" }
					]
				}), "filtersModel");
			this.getView().byId("Reg-Number").setValue("");
			this.getView().byId("filtersComboBox").setValue("Рег. Номер");
		},

		/**
		 * Shows the selected item on the detail page
		 * On phones a additional history entry is created
		 * @param {sap.m.ObjectListItem} oItem selected Item
		 * @private
		 */
		_showDetail: function (oItem) {
			var bReplace = !Device.system.phone;
			// set the layout property of FCL control to show two columns
			this.getModel("appView").setProperty("/layout", "MidColumnFullScreen");
			let torkey = oItem.getBindingContext().getProperty("TorKey")
			let stringifyObj = JSON.stringify([torkey, this.currentChosenWarehouseNumber])
			this.getRouter().navTo("object", {
				objectId: stringifyObj
			}, bReplace);
		},

		/**
		* Internal helper method to apply both filter and search state together on the list binding
		* @private
		*/
		_applyFilterSearch: function () {
			var aFilters = this._oTableFilterState.aSearch.concat(this._oTableFilterState.aFilter),
				oViewModel = this.getModel("masterView");
			this._oTable.getBinding("items").filter(aFilters, "Application");
			// changes the noDataText of the list in case there are no filter results
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataWithFilterOrSearchText"));
			} else if (this._oTableFilterState.aSearch.length > 0) {
				// only reset the no data text to default when no new search was triggered
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataText"));
			}
		},

		/**
		 * Internal helper method that sets the filter bar visibility property and the label's caption to be shown
		 * @param {string} sFilterBarText the selected filter value
		 * @private
		 */
		_updateFilterBar: function (sFilterBarText) {
			var oViewModel = this.getModel("masterView");
			oViewModel.setProperty("/isFilterBarVisible", (this._oTableFilterState.aFilter.length > 0));
			oViewModel.setProperty("/filterBarLabel", this.getResourceBundle().getText("masterFilterBarText", [sFilterBarText]));
		},

		quickFilterOnTable: function (oEvent, key) {
			const filters = {
				"pending": [new Filter("Status", "EQ", "01")],
				"confirmed": [new Filter("Status", "EQ", "02")],
				"all": []
			};

			let oBinding = this._oTable.getBinding("items");
			let filterKey = key ? key : "all"
			oBinding.filter(filters[filterKey]);
		},

		handleChangeComboBox: function (oEvent) {
			const oValidatedComboBox = oEvent.getSource();
			const sValue = oValidatedComboBox.getValue();
			const inputSearch = this.getView().byId("Reg-Number");
			let arrayOfItems = oEvent.getSource().getItems();

			for (let item in arrayOfItems) {
				if (sValue == arrayOfItems[item].getText()) {
					oValidatedComboBox.setValueState("None");
					inputSearch.setEnabled(true);
					break;
				} else {
					oValidatedComboBox.setValueState("Error");
					oValidatedComboBox.setValueStateText("Изберете филтър от листа!");
					inputSearch.setEnabled(false);
				}
			}
		},

		onComboBoxSelectionChangedWarehouseNumber: function () {
			const comboBox = this.getView().byId("warehouseComboBox");
			let newNumber = comboBox.getSelectedItem().getText();
			window.localStorage.setItem("selectedWhNumber", newNumber);
			this.currentChosenWarehouseNumber = newNumber;
		}

	});
});