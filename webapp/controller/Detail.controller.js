sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"../model/formatter",
	"sap/ui/core/Fragment",
	"sap/ui/model/odata/ODataModel",
], function (BaseController, JSONModel, formatter, Fragment, oDataModel) {
	"use strict";

	return BaseController.extend("zfioricheckin.controller.Detail", {

		formatter: formatter,
		getButtonsObject: function () {
			return {
				buttonWrite: this.getView().byId("buttonWrite"),
				buttonPrint: this.getView().byId("buttonPrint"),
				buttonChange: this.getView().byId("changeButton"),
				instructSwitch: this.getView().byId("instructSwitch"),
			}
		},

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */
		onInit: function () {
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data

			var oViewModel = new JSONModel({
				busy: false,
				delay: 0,
				lineItemListTitle: this.getResourceBundle().getText("detailLineItemTableHeading")
			});

			this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);

			this.setModel(oViewModel, "detailView");

			if (this.getOwnerComponent().getModel()) {
				this.getOwnerComponent().getModel().metadataLoaded().then(this._onMetadataLoaded.bind(this));
			}
		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */



		/**
		 * Updates the item count within the line item table's header
		 * @param {object} oEvent an event containing the total number of items in the list
		 * @private
		 */
		onListUpdateFinished: function (oEvent) {
			var sTitle,
				iTotalItems = oEvent.getParameter("total"),
				oViewModel = this.getModel("detailView");

			// only update the counter if the length is final
			if (this.byId("lineItemsList").getBinding("items").isLengthFinal()) {
				if (iTotalItems) {
					sTitle = this.getResourceBundle().getText("detailLineItemTableHeadingCount", [iTotalItems]);
				} else {
					//Display 'Line Items' instead of 'Line items (0)'
					sTitle = this.getResourceBundle().getText("detailLineItemTableHeading");
				}
				oViewModel.setProperty("/lineItemListTitle", sTitle);
			}
		},

		_onObjectMatched: function (oEvent) {
			sap.ui.core.BusyIndicator.show(0);
			var logModel = new JSONModel();
			this.getView().setModel(logModel, "logDetail");
			var oContext = new sap.ui.model.Context(logModel, "/");
			var selectedModel = this.getOwnerComponent().getModel("selected");
			const writeButton = this.getView().byId("buttonWrite");
			this.disableSertificateInputs()
			const arr = oEvent.getParameter("arguments").objectId;
			const parsed = JSON.parse(arr)
			const torKey = parsed[0]
			const localStorageNumber = parsed[1]
			this.removeCssFromTable();
			this.enableAllButtons();
			try {
				logModel.setData(selectedModel.oData);
				selectedModel.oData.Status === "02" ? writeButton.setEnabled(false) : writeButton.setEnabled(true);
			} catch (error) {
				console.warn("Nothing was selected. Going Back!");
				this.onCloseDetailPress();
			}

			this.getView().setBindingContext(oContext, "logDetail");
			this.getModel("appView").setProperty("/layout", "MidColumnFullScreen");

			const inputsODataModel = new sap.ui.model.odata.ODataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
			inputsODataModel.read("/MtrSearchHelpSet ", {
				success: data => {
					const oCustomNamedModel = new sap.ui.model.json.JSONModel();
					const aData = data.results;
					let customNamedKeyObj = {};
					customNamedKeyObj["inputModel"] = aData;
					oCustomNamedModel.setData(customNamedKeyObj);
					this.getView().setModel(oCustomNamedModel, "vehicles");

				}

			});

			inputsODataModel.read("/ResourceNameSet", {
				success: data => {
					const oCustomNamedModel = new sap.ui.model.json.JSONModel();
					const aData = data.results;
					let customNamedKeyObj = {};
					customNamedKeyObj["inputCarNumbersModel"] = aData;
					oCustomNamedModel.setData(customNamedKeyObj);
					this.getView().setModel(oCustomNamedModel, "vehiclesCarNumbers");

				}
			});

			this.getDataFromServer(localStorageNumber, torKey).then(data => {
				try {
					this.getView().setModel(new sap.ui.model.json.JSONModel(data.RegisterTruckHeaderNav), "header");
					this.getView().getModel("header").setDefaultBindingMode(sap.ui.model.BindingMode.TwoWay);
					this.getView().byId("uiTable").setModel(new sap.ui.model.json.JSONModel(data.RegisterTruckItemNav), "table");
					const mrtKey = data.RegisterTruckHeaderNav.Mtr;
					this.getView().byId("vehiclesComboBox").setSelectedKey(mrtKey);
					!this.doesNotNeedChecking() && this.colorIfHazard();
					// disable all buttons except print 	
					if (this.getView().byId("vehicleInput").getValue() && this.getView().byId("cardNumberInput").getValue()) {
						this.setEnabledButtons(false);
					}
					//this.getView().byId("vehiclesComboBox").setSelectedItem(new sap.ui.core.Item({ text: data.RegisterTruckHeaderNav.MtrTxt }));					
					sap.ui.core.BusyIndicator.hide();
				}
				catch (err) {
					console.log(err);
					sap.ui.core.BusyIndicator.hide();
				}

			}).catch(err => sap.ui.core.BusyIndicator.hide());

			this.getView().setModel(new JSONModel(
				{
					vehicleData: [
						{ val: "Car" },
						{ val: "Truck" },
						{ val: "Bus" }
					]
				}), "vehiclesModel");
		},

		colorIfHazard: function () {
			setTimeout(() => {
				const table = this.getView().byId("uiTable");
				let rows = table.getRows();
				let harardCellCount = 6;
				rows.forEach(row => {
					let currentText = row.getCells()[harardCellCount].getText();
					if (currentText === "Да") {
						row.addStyleClass("red-row");
					} else {
						row.removeStyleClass("red-row");
					}
				});
			}, 0)
		},

		/**
		 * Binds the view to the object path. Makes sure that detail view displays
		 * a busy indicator while data for the corresponding element binding is loaded.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound to the view.
		 * @private
		 */
		_bindView: function (sObjectPath) {
			// Set busy indicator during view binding
			var oViewModel = this.getModel("detailView");

			// If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
			oViewModel.setProperty("/busy", false);

			this.getView().bindElement({
				path: sObjectPath,
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function () {
						oViewModel.setProperty("/busy", true);
					},
					dataReceived: function () {
						oViewModel.setProperty("/busy", false);
					}
				}
			});
		},

		_onBindingChange: function () {
			var oView = this.getView(),
				oElementBinding = oView.getElementBinding();

			// No data for the binding
			if (!oElementBinding.getBoundContext()) {
				this.getRouter().getTargets().display("detailObjectNotFound");
				// if object could not be found, the selection in the master list
				// does not make sense anymore.
				this.getOwnerComponent().oListSelector.clearMasterListSelection();
				return;
			}

			var sPath = oElementBinding.getPath(),
				oResourceBundle = this.getResourceBundle(),
				oObject = oView.getModel().getObject(sPath),
				sObjectId = oObject.ObjectID,
				sObjectName = oObject.Name,
				oViewModel = this.getModel("detailView");

			this.getOwnerComponent().oListSelector.selectAListItem(sPath);

			oViewModel.setProperty("/shareSendEmailSubject",
				oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId]));
			oViewModel.setProperty("/shareSendEmailMessage",
				oResourceBundle.getText("shareSendEmailObjectMessage", [sObjectName, sObjectId, location.href]));
		},

		_onMetadataLoaded: function () {
			// Store original busy indicator delay for the detail view
			var iOriginalViewBusyDelay = this.getView().getBusyIndicatorDelay(),
				oViewModel = this.getModel("detailView"),
				oLineItemTable = this.byId("lineItemsList"),
				iOriginalLineItemTableBusyDelay = oLineItemTable.getBusyIndicatorDelay();

			// Make sure busy indicator is displayed immediately when
			// detail view is displayed for the first time
			oViewModel.setProperty("/delay", 0);
			oViewModel.setProperty("/lineItemTableDelay", 0);

			oLineItemTable.attachEventOnce("updateFinished", function () {
				// Restore original busy indicator delay for line item table
				oViewModel.setProperty("/lineItemTableDelay", iOriginalLineItemTableBusyDelay);
			});

			// Binding the view will set it to not busy - so the view is always busy if it is not bound
			oViewModel.setProperty("/busy", true);
			// Restore original busy indicator delay for the detail view
			oViewModel.setProperty("/delay", iOriginalViewBusyDelay);
		},

		/**
		 * Set the full screen mode to false and navigate to master page
		 */
		onCloseDetailPress: function () {
			this.getModel("appView").setProperty("/actionButtonsInfo/midColumn/fullScreen", false);
			// No item should be selected on master after detail page is closed
			this.getOwnerComponent().oListSelector.clearMasterListSelection();
			this.getRouter().navTo("master");
		},

		/**
		 * Toggle between full and non full screen mode.
		 */
		toggleFullScreen: function () {
			var bFullScreen = this.getModel("appView").getProperty("/actionButtonsInfo/midColumn/fullScreen");
			this.getModel("appView").setProperty("/actionButtonsInfo/midColumn/fullScreen", !bFullScreen);
			if (!bFullScreen) {
				// store current layout and go full screen
				this.getModel("appView").setProperty("/previousLayout", this.getModel("appView").getProperty("/layout"));
				this.getModel("appView").setProperty("/layout", "MidColumnFullScreen");
			} else {
				// reset to previous layout
				this.getModel("appView").setProperty("/layout", this.getModel("appView").getProperty("/previousLayout"));
			}
		},

		onWritePressed: function () {
			const buttonWrite = this.getView().byId("buttonWrite");
			buttonWrite.setEnabled(false)
			setTimeout(() => buttonWrite.setEnabled(true))
			const datePick = this.getView().byId("datePick");
			const vehiclesComboBox = this.getView().byId("vehiclesComboBox");
			const sw = this.getView().byId("instructSwitch");
			const vehicleText = this.getView().byId("vehiclesComboBox").getSelectedItem() && this.getView().byId("vehiclesComboBox").getSelectedItem().getText();
			const numberTrailerText = this.getView().byId("vehicleInput").getValue();
			const cardNumberText = this.getView().byId("cardNumberInput").getValue();
			const sertificateInputValid = this.getView().byId("sertificateInput").getValue() !== "" ? true : false;
			if (sw.getState()) {
				if (vehicleText && numberTrailerText && cardNumberText) {
					if (this.doesNotNeedChecking()) {
						//sap.m.MessageToast.show("Няма проверка!");
					}
					else if (this.isStockHazard() && (!sertificateInputValid || this.isDateInvalid(datePick.getDateValue()))) {
						sap.m.MessageToast.show("Товара е опасен и сертификатът е изтекъл или не е въведен!");
						return;
					}
					const oDataModel = new sap.ui.model.odata.ODataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
					const headerModel = this.getView().getModel("header");
					let payload = headerModel.getData();

					payload.AdrValto = datePick.getDateValue();
					vehiclesComboBox.getModel("vehicles").getData().inputModel.forEach(item => {
						if (item.MtrTxt === vehiclesComboBox.getValue()) {
							payload.Mtr = item.Mtr;
							payload.MtrTxt = item.MtrTxt;
						}
					});
					sap.ui.core.BusyIndicator.show(0);
					window.setTimeout(() => {
						oDataModel.create("/RegisterTruckHeaderSet", payload, {
							success: () => {
								this.setEnabledButtons(false);
								sap.ui.core.BusyIndicator.hide();
								sap.m.MessageToast.show("Успешно изпратени Данни!");
							},
							error: err => {
								sap.ui.core.BusyIndicator.hide();
								if (err) {
									err.response.body && sap.m.MessageBox.error(JSON.parse(err.response.body).error.message.value);
								}
							}
						});

					}, 0);
				} else {
					sap.m.MessageToast.show("Въведи Превозно средство, Рег. Номер Ремарке и Карта Номер!");
				}
			}
			else {
				sap.m.MessageToast.show("Не е проведен инструктаж!")
			}
		},

		isStockHazard: function () {

			const table = this.getView().byId("uiTable");
			let rows = table.getRows();
			let harardCellCount = 6;
			for (let element of rows) {
				let currentText = element.getCells()[harardCellCount].getText();
				if (currentText === "Да") {
					return true;
				}
			}
			return false;
		},

		isDateInvalid: function (pickedDate) {
			if (pickedDate) {
				var today = new Date();
				pickedDate.setDate(pickedDate.getDate() + 1);
				return pickedDate < today;
			} else {
				return true;
			}
		},

		doesNotNeedChecking: function () {
			const table = this.getView().byId("uiTable");
			const model = table.getModel("table");
			if (model.getData()) {
				if (model.getData().results[0].EwmDoccat === "PDO") {
					return false;
				} else {
					return true
				}
			}
			return true;
		},


		onChangeRowPressed: function () {
			const sertInput = this.getView().byId("sertificateInput");
			const datePicker = this.getView().byId("datePick");

			sertInput.setEnabled(true);
			datePicker.setEnabled(true);
		},

		disableSertificateInputs: function () {
			const sertInput = this.getView().byId("sertificateInput");
			const datePicker = this.getView().byId("datePick");

			sertInput.setEnabled(false);
			datePicker.setEnabled(false);
		},



		getDataFromServer: function (lgNum, torKey) {
			return new Promise((resolve, reject) => {

				const oDataModel = new sap.ui.model.odata.ODataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
				let setInfo = "(Lgnum='" + lgNum + "',TorKey=guid'" + torKey + "')"
				oDataModel.read("/RegisterTruckGenSet" + setInfo, {
					urlParameters: {
						"$expand": "RegisterTruckHeaderNav,RegisterTruckItemNav"
					},
					success: (s, response) => {
						console.log(response);
						resolve(s);
						let sapMessage = response.headers["sap-message"];
						this.getView().byId("buttonWrite").setEnabled(true);
						if (sapMessage) {
							sap.m.MessageBox.alert(JSON.parse(sapMessage).message);
							this.getView().byId("buttonWrite").setEnabled(false);
						}
					},
					error: err => reject(err)
				});
			});
		},

		onPrintPressed: function () {
			this.onOpenPrintDialog();
		},

		onPrintPressedRequest: function (event, dataArr) {
			const oModel = new sap.ui.model.odata.ODataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
			const headerModel = this.getView().getModel("header");
			const tableModelData = this.getView().byId("uiTable").getModel("table").getData();
			const datePick = this.getView().byId("datePick");
			const vehiclesComboBox = this.getView().byId("vehiclesComboBox");
			let headerModelUpdated = headerModel.getData();
			let requestBody = {}

			vehiclesComboBox.getModel("vehicles").getData().inputModel.forEach(item => {
				if (item.MtrTxt === vehiclesComboBox.getValue()) {
					headerModelUpdated.Mtr = item.Mtr;
					headerModelUpdated.MtrTxt = item.MtrTxt;
				}
			});
			headerModelUpdated.AdrValto = datePick.getDateValue();

			requestBody.RegisterTruckHeaderNav = headerModelUpdated;
			requestBody.RegisterTruckItemNav = tableModelData;
			requestBody.RegisterTruckPrintNav = dataArr ? dataArr : []
			oModel.create("/RegisterTruckGenSet", requestBody, {
				success: succ => console.log(succ)
			});
		},

		enableAllButtons: function () {
			let obj = this.getButtonsObject();
			for (let button in obj) {
				if (obj.hasOwnProperty(button)) {
					obj[button].setEnabled(true);
				}
			}
		},

		removeCssFromTable: function () {
			const cssClasses = { "03": "red-row", "02": "yellow-row" }
			const table = this.getView().byId("uiTable");
			table.getRows().forEach(row => {
				row.removeStyleClass(cssClasses["02"]);
				row.removeStyleClass(cssClasses["03"]);
			});
		},

		setEnabledButtons: function (enabled) {
			let obj = this.getButtonsObject();
			for (let button in obj) {
				if (button !== "buttonPrint") {
					if (obj.hasOwnProperty(button)) {
						obj[button].setEnabled(enabled);
					}
				}
			}
		},


		onOpenPrintDialog: function () {
			const view = this.getView();
			const popupDialogPrint = this.byId("popupDialogPrint")
			if (!popupDialogPrint) {
				Fragment.load({
					id: view.getId(),
					name: "zfioricheckin.view.dialogs.popupDialogPrint",
					controller: this
				}).then((popupDialogPrint) => {
					view.addDependent(popupDialogPrint);
					popupDialogPrint.open();
				});
			} else {
				this.byId("popupDialogPrint").open();
			}
		},

		afterOpenPrintDialog: function () {

			let applId = new sap.ui.model.Filter("ApplId", "EQ", "CHECK_IN");

			const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
			oModel.read("/LabelPrintCollection", {
				filters: [applId],
				success: (s, res) => {
					sap.ui.core.BusyIndicator.hide();
					this.getView().byId("uiTablePrint").setModel(new JSONModel(s), "tablePrint")

				},
				error: err => {
					sap.ui.core.BusyIndicator.hide(0);
					this.handleMessageResponse(err);
				}
			});
		},

		onClosePrint: function () {

			const table = this.getView().byId("uiTablePrint")
			const dataArr = table.getModel("tablePrint").getData().results;
			const selectedIndices = table.getSelectedIndices();
			let arr = [];
			selectedIndices.forEach(index => {
				let itemToAdd = dataArr[index];
				arr.push(itemToAdd);
			});
			//this.onEventButtonPressed("03", false, arr);
			this.onPrintPressedRequest(null, arr)
			this.byId("popupDialogPrint").close()
		},

		onCloseNoActionPrint: function () {
			this.byId("popupDialogPrint").close()
		}
	});

});